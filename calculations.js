function Calculations(receiver)
{
    var self=this;
    if (receiver) this.receiver=receiver;

    // 2014
    //var forfaitperc = 0.75 / 100

    // 2015
    // box 1
    var forfaitperc = 0.75 / 100
    var s0perc  = 36.5 / 100;
    var schaal0 = 19822;    // higher is s1perc
    var s1perc  = 42 / 100;
    var schaal1 = 57586;    // higher is s2perc
    var s2perc = 52 / 100;

	// general parameters
    var restant = 161854.14;
    var woz = 197000 // beschikking 2015
    woz = 185000 // gehanteerd door robin
	var jaarloon=50000;
	jaarloon=85526;
	//jaarloon = 100000

    var forfait = woz * forfaitperc
    var date = new Date();
    var dyear = date.getYear();
    var dmon = date.getMonth();

    var mleft = (2032 - 1900) - dyear;
    mleft = mleft * 12 + dmon;

    self.mortgage=function(aflos,perc,kapitaalverz,kosten) {

		restant -= aflos;
        var yrly = restant * perc / 100
        var interest = Math.round(yrly*100/12)/100
        var monthly = interest + kapitaalverz;

        txt = "<td>" + Math.round(monthly) + "<td>"
        txt += Math.round((yrly + 12 *kapitaalverz*100))/100 + "   "
		txt += "<td>"
        txt += perc
        if (perc.toString().length == 3) txt += "0"
		txt += "<td>"
        txt += forfait + "<td>"
        txt += Math.round(interest);
		txt += "/"
        txt += Math.round(kapitaalverz);
		txt += "<td>"
        txt += Math.round(12 * (interest + kapitaalverz) );
		txt += "<td>";
		voordeel = Math.round(self.tax_deduction(interest*12)/12)
		txt += Math.round(interest + kapitaalverz) 
		txt += "/";
		txt += voordeel
		txt += "/";
		txt += Math.round(interest + kapitaalverz) - voordeel
        txt += "<td>";
        txt += Math.round(mleft * (interest + kapitaalverz) + kosten );
        txt += "<br>"
        return txt;
    }

    self.tax_deduction=function (interest)
    {
		return self.tax(jaarloon) - self.tax(jaarloon-interest+forfait);
    }

    self.tax=function (total)
    {
        var x = total * s0perc;
        if (total > schaal0) 
            x = schaal0 * s0perc;
        if (total > schaal1) 
            x += (schaal1-schaal0) * s1perc;
        else 
            return x + s1perc * (total-schaal0);
        x += s2perc * (total-schaal1);

        return x;
    } 

    self.headers=function() { 
        general += "<td>Maand    <td>jaar      <td>perc  <td>forfait <td>maand   <td>jaar <td>voordeel <td>totaal<br>";
        return general;
    } 

    text = "<pre><table border=1 cellspacing=0px cellpadding=0px>"
    general = "Restant lening : " + restant + " in " + mleft + " months: " + self.tax(jaarloon)+"<br>"
	text += "<tr>"
    text += self.headers();
	text += "<tr>"
    text += self.mortgage(0,3.10, 149.72, 0);
	text += "<tr>"
    text += self.mortgage(0,2.30, 149.72, 0);
	text += "<tr>"
    text += self.mortgage(0,2.45, 125,    4871);
	text += "<tr>"
    text += self.mortgage(40000,3.10, 149.72, 0);
	text += "<tr>"
    text += "</table></pre>"

    self.receiver.html(text);
}
