package transactions;

import java.io.*;
import java.util.Date;

class Transaction
{
    String account;
    String currency;
    Date transaction_date;
    Date book_date;
    String book_code;
    boolean debet;
    float amount;
    String opp_account;
    String opp_name;
    String descr[];

    Transaction(String acc, String cur, String td, String bd, String bc)
    {
        descr = new String[5];
    }

    Transaction(String line)
    {
        String regexp = "\",\"";
        String[] tokens = line.split(regexp, -1);
        System.out.println(tokens.length);
    }
    
//"NL09RABO0323228240","EUR","20140110","D","20.38","","","20140110","ba","","183 Lidl H v Holland HOEK VAN HOLL","Betaalautomaat 18:45 pasnr. 015","","","","","","",""
};

public class Transactions {

    Transaction tarr[];

    static void HandleFile(File f) { 
        String content =null;
        try { 
            FileReader reader = new FileReader(f);
            char [] chars = new char[(int) f.length()];
            reader.read(chars);
            content = new String(chars);
            reader.close();
            String lines[] = content.split("\n");
            for (String line : lines) { 
                Transaction t = new Transaction(line);
            } 
            System.out.println(lines.length);
        } catch (IOException e) { 
            e.printStackTrace();
        } 
    } 

    static void ReadFiles(final File folder) {
        for (final File fileEntry : folder.listFiles()) {
            if (fileEntry.isDirectory()) {
                ReadFiles(fileEntry);
            } else {
                HandleFile(fileEntry);
                //System.out.println(fileEntry.getName());
            }
        }
    }

    static public final void main(String a[]) {
        System.out.println("Start transaction filter : ");

        //final File folder = new File("data");
        //ReadFiles(folder);

        final File trans = new File("data/transactions.txt");
        HandleFile(trans);
    } 
}
