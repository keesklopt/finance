function Transaction(line)
{
    var columns = [
        "account",  // 0
        "valuta",
        "rentedatum",
        "creditcode",
        "amount",
        "tegenrekening",
        "naar_naam",
        "boekdatum",
        "boekcode",
        "filler",   // 09
        "descr1",   // 6 desciption lines
        "descr2",
        "descr3",
        "descr4",
        "descr5",
        "descr6",
        "end_to_end_id",    // three SEPA fields (??)
        "id_tegenrekeningouder",
        "mandaat_id"       // 18
    ];

    this.fields = [];

    for (y in line) {
        l = line[y];
        k = columns[y];
        this.fields[k] = l;
    } 

    this.get=function(key) {
        return this.fields[key];
    } 
}

function Transactions(receiver)
{
    var self=this;
    if (receiver) this.receiver=receiver;
    self.transactions = [];

    self.balance=function(anr,start)
    {
        for (x in self.transactions) { 
            var t = self.transactions[x];
            var dt = t.get("datum")
            var amount = parseFloat(t.get("amount"));
            var nr = t.get("account");
            if (nr != anr) continue;
            //console.log("s:" + start);
            //console.log("a:" + amount);
            if (t.get("creditcode")=='D') start -= amount;
            else start += amount;
        } 
        return Math.round( start *100) /100;
    }

    self.get=function() {
        var result = $.getJSON('resources/transactions',
            function (jdata) {
                //console.log(jdata)
                data = eval(jdata);
                txt = "";
                for (x in data.arraycontent) {
                    line = data.arraycontent[x];
                    self.transactions.push(new Transaction(line));
                } 
                txt = "Rabo betaal : " + self.balance("NL09RABO0323228240",2424.57);
                txt += "<br>Rabo spaar  : " + self.balance("NL09RABO3232432743",21534.26);
                txt += "<br>Knab zakelijk: " + self.balance("NL09RABO3232432743",21534.26);
                self.receiver.html(txt);
            }
        )
        .fail(function(jqxhr,textStatus,error) { 
                console.log(error);
            }
        )
    } 

    self.get();
}
