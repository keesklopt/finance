from transactions import Transactions

def dispatch(environ):
    method = environ.get('METHOD','');
    print (method);
    path = environ.get('PATH_INFO','');
    if (path==''): return False;
    script = environ.get('SCRIPT_NAME','');
    
    if (path == '/transactions'):
        t = Transactions()
        return t.get()

    return path
