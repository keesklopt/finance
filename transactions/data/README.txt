two older transactions lists have been downloaded earlier but they do not overlap. 
transactions_2012_2013.txt : stops on 11 december 2013
transactions_2014.txt      : starts in 10 jan 2014

That's a gap, so don't use these. 

transactions_2014.txt and transactions_base.txt DO overlap so these have been merged into transactions_base.txt.
transactions_base.txt has been copied to transactions.txt in juli 2015
and all new transactions.txt files have been added since :

. transactions_20150715.txt
. transactions_20150726.txt

So .... Use transactions.txt and cat a new transactions.txt and give it a sensible name. In short the last file was added like this :

    vimdiff transactions.txt transactions_20150715.txt
    wget https://klopt.org/rest/transactions
    mv transactions.1 resources/transactions

Browse to https://klopt.org/finance and run the transactions tabs, the amounts should match or there is a gap somewhere.

For this setup we need to have start amounts 2525.57 and 21534.26 
( in transactions.js) : 

txt = "balance now : " + self.balance("NL09RABO0323228240",2424.57);
txt += "<br>balance now : " + self.balance("NL09RABO3232432743",21534.26);

