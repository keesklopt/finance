#!/usr/bin/python
import csv
from sets import Set

class Transactions():
    # at least backslashes should be escaped, 
    # but it think it looks ugly, remove them !
    def clean(self,val):
        return val.replace("\\","")

        
    def get(self):
        json ='{ "arraycontent" : ['
        # don't skip, just fill in the complete structure
        #skip = Set([1,2,7,8,9,10,11,12,13,14,15]) # 1-based !!
        with open("/var/www/html/finance/transactions/data/transactions.txt", "rb") as csvfile:
            tareader = csv.reader(csvfile,delimiter=',', quotechar='"');
            r=0
            for row in tareader:
                if (r>0): json += ","
                r += 1
                comma=0
                c=0
                json += "["
                for col in row:
                    if (comma>0): json += ","
                    c = c + 1
                    #if c in skip: continue
                    comma = comma+1
                    json += "\"" + self.clean(col) + "\""
                json += "]"
        json += "]}"
        return json

#read_transactions()
