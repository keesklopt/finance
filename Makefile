all: package

base_dir=/tmp/fpm
inst_dir=$(base_dir)/usr/share/klopt
conf_dir=$(base_dir)/etc/apache2/sites-available
web_dir=/var/www/html/repo
pack_dir=~/projects/packages
version=1.0

package:
	mkdir -p $(inst_dir)/finance/html
	mkdir -p $(conf_dir)
	cp -r . $(inst_dir)/finance/html
	cp finance.conf $(conf_dir)
	rm -f *.deb
	fpm -s dir -t deb -n "klopt-finance" \
		--after-install finance_post.sh	\
		-v ${version}				\
		-C $(base_dir)  .
	rm -rf /tmp/fpm

provision:
	cp *.deb $(pack_dir)

# su first !
install: 
	cp *.deb $(web_dir)/klopt
	cd $(web_dir); dpkg-scanpackages klopt /dev/null | gzip -9c > klopt/Packages.gz
