import sys, os
# to include submodule in this directory:
sys.path.append(os.path.dirname(__file__))
import jsonrpc

def application(environ, start_response):
    status = '200 OK'
    output = jsonrpc.dispatch(environ)

    response_headers = [('Content-type', 'text/plain'),
                        ('Content-Length', str(len(output)))]
    start_response(status, response_headers)
    return output;

# and this one will only be called on the command line
if __name__ == '__main__':
    print ("Standalone test page ------------")
    output = jsonrpc.dispatch(environ)
    print output
